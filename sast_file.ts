// This is High
textBox.innerHTML = snarkdown(hint.text)

// This is Medium
tableData[i].description = this.sanitizer.bypassSecurityTrustHtml(tableData[i].description)

// This is Low
encodeProductDescription (tableData: any[]) {
    for (let i = 0; i < tableData.length; i++) {
      tableData[i].description = tableData[i].description.replaceAll('<', '&lt;').replaceAll('>', '&gt;')
    }
  }